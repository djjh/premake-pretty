-- pretty.lua
-- The pretty Premake Module.

package.path = "dep/ansicolors.lua/?.lua;" .. package.path

local ac = require("ansicolors")
local colors =
{
    cyan    = '%{bright cyan}';
    magenta = '%{bright magenta}';
    yellow  = '%{bright yellow}';
    red     = '%{bright red}';
    green   = '%{bright green}';
    blue    = '%{bright blue}';
    white   = '%{bright white}';
    [true]  = '%{bright green}';
    [false] = '%{bright red}';
}

local m = {}

m._VERSION = "0.0.0"

newoption
{
    trigger = "quiet",
    description = "Quieter printing.",
}

local _print = _G.print
if _OPTIONS.quiet then
    _G.print = function() end
end

local function format_tag(key)
    local opts =
    {
        ["error"]   = ac(colors.red     .. "ERROR"),
        ["debug"]   = ac(colors.magenta .. "DEBUG"),
        ["info"]    = ac(colors.cyan    .. "INFO"),
        ["success"] = ac(colors.green   .. "SUCCESS"),
    }

    local max_len = 0
    for _,v in pairs(opts) do
        max_len = math.max(max_len, #v)
    end

    local tag = opts[key]
    if tag then
        local pad = math.max(0, max_len - #tag)
        local pad_char = " "
        return string.format("[%s]%s", tag, pad_char:rep(pad))
    end
end

function m.print_header(...)
    local width = 80;
    local header = ac(colors.blue .. string.rep("=", width))
    print(header)
    local title = string.format(...);
    local width_diff = width - string.len(title);
    if width_diff > 0 then
        -- center the title
        title = string.rep(" ", width_diff // 2 + width_diff % 2) .. title
    end
    print(title)
    print(header)
end

function m.print_divider()
    local header = string.rep("=", 80)
    print(header)
end

function m.print_newline(n)
    print(("\n"):rep((n or 1) - 1))
end

function m.printf(s, ...)
    print(string.format(s or "", select(1, ...)))
end

function m.print_debug(...)
    print(format_tag("debug"), string.format(...))
end

function m.print_error(...)
    print(format_tag("error"), string.format(...))
    --print_debug(debug.traceback())
    os.exit(1)
end

function m.print_info(...)
    print(format_tag("info"), string.format(...))
end

function m.print_success(...)
    print(format_tag("success"), string.format(...))
end

function m.assert_notnil(val, msg, f)
    if type(val) == "nil" then
        if type(f) == "function" then
            f()
        end

        if msg then
            m.print_error(msg)
        else
            os.exit(1)
        end
    end
    return val
end

function m.assert_true(cond, msg, f)
    if cond ~= true then
        if type(f) == "function" then
            f()
        end

        if msg then
            m.print_error(msg)
        else
            os.exit(1)
        end
    end
    return cond
end

function m.assert_return_code(code, msg, f)
    if code ~= 0 then
        if type(f) == "function" then
            f()
        end

        if msg then
            m.print_error(msg)
        else
            os.exit(1)
        end
    end
    return code
end

function m.enquote(s)
    return ([["%s"]]):format(s)
end

return m
